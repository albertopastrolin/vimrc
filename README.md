* Requires vim-packager (https://github.com/kristijanhusak/vim-packager) if you are using init.vim
* Requires paq-nvim (https://github.com/savq/paq-nvim) if you are using init.lua

* init.vim has been tested with neovim but should work also with vim 8
* init.lua requires a nightly build of neovim 0.5 to use the integrated lsp client
