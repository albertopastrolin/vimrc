local fn = vim.fn    -- to call Vim functions e.g. fn.bufnr()
local cmd = vim.cmd

-------------------- TREE-SITTER ---------------------------
local ts = require 'nvim-treesitter.configs'
ts.setup {ensure_installed = 'maintained', highlight = {enable = true}}

-------------------- LSP -----------------------------------
local lsp = require 'lspconfig'
local lspfuzzy = require 'lspfuzzy'

-- Load the lsp servers
-- remember to install the corresponding packages
-- For more info see https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md

lsp.dockerls.setup {}      -- Dockerfile language server
lsp.html.setup {}          -- HTML language server
lsp.jsonls.setup {}        -- JSON language server
lsp.solargraph.setup {}    -- Ruby language server
lsp.stylelint_lsp.setup {} -- CSS/SCSS language server
lsp.tsserver.setup {
  on_attach = function(_, bufnr)
    local ts_utils = require("nvim-lsp-ts-utils")

    -- defaults
    ts_utils.setup {
      disable_commands = false,
      enable_import_on_completion = false,
      import_on_completion_timeout = 5000,
      -- eslint
      eslint_bin = "eslint_d",
      eslint_args = {"-f", "json", "--stdin", "--stdin-filename", "$FILENAME"},
      eslint_enable_disable_comments = true,

      -- experimental settings!
      -- eslint diagnostics
      eslint_enable_diagnostics = true,
      eslint_diagnostics_debounce = 250,
      -- formatting
      enable_formatting = false,
      formatter = "prettier_d_slim",
      formatter_args = {"--stdin-filepath", "$FILENAME"},
      format_on_save = false,
      no_save_after_format = false
    }
  end
}                          -- TypeScript/JavaScript language server
lsp.vuels.setup {
  -- root_dir is where the LSP server will start: here at the project root otherwise in current folder
  root_dir = lsp.util.root_pattern('package.json', fn.getcwd()),
  on_attach = function(client)
    client.resolved_capabilities.document_formatting = true
  end
}                          -- Vue language server
lspfuzzy.setup {}          -- Make the LSP client use FZF instead of the quickfix list

-- LSP warning messages configuration
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics,
  {
    virtual_text = false,
    underline = true,
    signs = true
  }
)

-- Automatically show the lsp diagnostics when you move the cursor above a symbol
cmd [[autocmd CursorMoved * Lspsaga show_line_diagnostics]]
cmd [[autocmd CursorMovedI * silent! Lspsaga signature_help]] 
