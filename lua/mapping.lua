local function map(mode, lhs, rhs, opts)
  local options = {noremap = true}
  if opts then options = vim.tbl_extend('force', options, opts) end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-------------------- MAPPINGS ------------------------------
map('i', '<C-t>', ':enew<CR>')                                             -- New buffer mapping
map('i', '<C-u>', '<C-g>u<C-u>')                                           -- Make <C-u> undoable
map('i', '<C-w>', '<C-g>u<C-w>')                                           -- Make <C-w> undoable
map('i', '<S-Tab>', 'pumvisible() ? "\\<C-p>" : "\\<Tab>"', {expr = true}) -- <Tab> to navigate the completion menu
map('i', '<Tab>', 'pumvisible() ? "\\<C-n>" : "\\<Tab>"', {expr = true})   -- <Tab> to navigate the completion menu
map('n', 'gT', '<cmd>BufferLineCyclePrev<CR>')                             -- Previous buffer
map('n', 'gt', '<cmd>BufferLineCycleNext<CR>')                             -- Next buffer
map('n', '<C-Down>', 'gt')                                                 -- Previous buffer
map('n', '<C-Up>', 'gT')                                                   -- Next buffer
map('n', '<C-b>', '<cmd>Buffers<cr>')                                      -- Buffers list
map('n', '<C-l>', '<cmd>noh<CR>')                                          -- Clear highlights
map('n', '<C-p>', '<cmd>Files<cr>')                                        -- Files present in the current directory
map('n', '<C-t>', '<cmd>enew<CR>')                                         -- New buffer mapping
map('n', '<CR>', '<cmd>noh<CR><CR>')                                       -- This unsets the last search pattern register by hitting return
map('n', '<leader>l', '<cmd>Loremipsum<CR>')                               -- Inserts a placeholder text
map('n', '<leader>o', 'm`o<Esc>``')                                        -- Insert a newline in normal mode
map('n', '<Leader>p', '<cmd>call contabs#project#select()<CR>')            -- Open the contabs menu to quickly switch project

---- Lsp Mappings ----
map('n', '<leader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>')             -- Refactor the current buffer using LSP diagnostics
map('n', '<space>m', '<cmd>lua vim.lsp.buf.rename()<CR>')                  -- Rename a symbol
map('n', '<space>r', '<cmd>lua vim.lsp.buf.references()<CR>')              -- Search all references of the current symbol
map('n', '<space>s', '<cmd>lua vim.lsp.buf.document_symbol()<CR>')         -- Show all the symbols found in the current buffer


---- LSPsaga Mappgins ---
map('n', '<leader>cc', '<cmd>:Lspsaga show_cursor_diagnostics<CR>')        -- LSP diagnostics of the current symbol
map('n', '<leader>cd', '<cmd>:Lspsaga show_line_diagnostics<CR>')          -- LSP diagnostics of the current line
map('n', '<space>b', '<cmd>:Lspsaga diagnostic_jump_prev<CR>')             -- Quick jump to the previous diagnostic message
map('n', '<space>n', '<cmd>:Lspsaga diagnostic_jump_next<CR>')             -- Quick jump to the next diagnostic message
map('n', '<space>a', ':Lspsaga code_action<CR>')                           -- code action
map('n', '<space>d', ':Lspsaga preview_definition<CR>')                    -- Preview function definition
map('n', '<space>h', ':Lspsaga hover_doc<CR>')                             -- show hover doc
map('n', '<space>gs', ':Lspsaga signature_help<CR>')                       -- show signature help
map('v', '<space>a', ':<C-U>Lspsaga range_code_action<CR>')                -- code action
