local cmd = vim.cmd  -- to execute Vim commands e.g. cmd('pwd')
local has = vim.fn.has
--  vim.o for setting global options, vim.bo for setting buffer-scoped options and vim.wo for setting window-scoped options
local scopes = {o = vim.o, b = vim.bo, w = vim.wo}

-- Workaround until we don't have vim.opt available (see https://github.com/neovim/neovim/pull/13479)
local function opt(scope, key, value)
  scopes[scope][key] = value
  if scope ~= 'o' then scopes['o'][key] = value end
end

-------------------- OPTIONS -------------------------------
local indent = 2
cmd 'colorscheme tokyonight'
vim.g.tokyonight_style = 'night'
opt('b', 'expandtab', true)                  -- Use spaces instead of tabs
opt('b', 'shiftwidth', indent)               -- Size of an indent
opt('b', 'smartindent', true)                -- Insert indents automatically
opt('b', 'softtabstop', indent)              -- Number of spaces soft tabs count for
opt('b', 'tabstop', indent)                  -- Number of spaces tabs count for
opt('b', 'undofile', true)                   -- Enable undofiles
opt('o', 'background', 'dark')               -- Sets the background to dark
opt('o', 'completeopt', 'menuone,noselect')  -- Autocomplete menu
opt('o', 'hidden', true)                     -- Enable modified buffers in background
opt('o', 'ignorecase', true)                 -- Ignore case
opt('o', 'joinspaces', false)                -- No double spaces with join after a dot
opt('o', 'mouse', 'a')                       -- Enable mouse
opt('o', 'scrolloff', 4 )                    -- Lines of context
opt('o', 'shiftround', true)                 -- Round indent
opt('o', 'sidescrolloff', 8 )                -- Columns of context
opt('o', 'smartcase', true)                  -- Don't ignore case with capitals
opt('o', 'splitbelow', true)                 -- Put new windows below current
opt('o', 'splitright', true)                 -- Put new windows right of current
opt('o', 'termguicolors', true)              -- True color support
opt('o', 'wildmode', 'longest:full,full')    -- Command-line completion mode
opt('w', 'number', true)                     -- Print line number
opt('w', 'relativenumber', true)             -- Relative line numbers
opt('w', 'wrap', false)                      -- Disable line wrap

-- Use system clipboard
if has('unix')
  then opt('o', 'clipboard', 'unnamedplus')  -- Use unnamedplus in linux
elseif has('macunix') 
  then opt('o', 'clipboard', 'unnamed')      -- Use unnamed in osx
end
