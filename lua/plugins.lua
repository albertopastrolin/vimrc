local cmd = vim.cmd  -- to execute Vim commands e.g. cmd('pwd')
local fn = vim.fn    -- to call Vim functions e.g. fn.bufnr()
local g = vim.g      -- a table to access global variables
local nvim_exec = vim.api.nvim_exec

-------------------- PLUGINS -------------------------------
cmd 'packadd paq-nvim'               -- load the package manager
-- Aliases the commmands to automate loading/installing/updating/cleaning
local paqNvim = require('paq-nvim')
local paq = paqNvim.paq
local install = paqNvim.install
local update = paqNvim.update
local clean = paqNvim.clean
paq {'savq/paq-nvim', opt = true}    -- paq-nvim manages itself

-------------------- Utilities -------------------------------
paq {'nvim-lua/plenary.nvim'}                                               -- Lua utilities
paq {'alvan/vim-closetag'}                                                  -- Autclose HTML
paq {'chaoren/vim-wordmotion'}                                              -- Better word movements for CamelCase and snake_case
paq {'cohama/lexima.vim'}                                                   -- Auto close parentheses
paq {'itchyny/vim-cursorword'}                                              -- Highlight the current word in the buffer
paq {'mbbill/undotree'}                                                     -- Undotree
paq {'mcchrish/nnn.vim'}                                                    -- Nnn file manager integration
paq {'pechorin/any-jump.vim'}                                               -- Jump to definitions/references
paq {'rhysd/git-messenger.vim'}                                             -- Git commit info directly in vim
paq {'hrsh7th/nvim-compe'}                                                  -- Autocompletion engine
paq {'vim-scripts/loremipsum'}                                              -- Quick lorem ipsum script

-------------------- Fzf -------------------------------
paq {'junegunn/fzf', run = fn['fzf#install']}                               -- Interactive list filter
paq {'junegunn/fzf.vim'}                                                    -- Fzf integration for vim
paq {'ojroques/nvim-lspfuzzy'}                                              -- Integrate the lsp client results with fzf
paq {'m00qek/nvim-contabs'}                                                 -- Integrate tabs workspaces with fzf

------------------------------- Themes -----------------------------------
paq {'folke/tokyonight.nvim'}
-- paq {'romainl/flattened'}                                                   -- Solarized, without the bullshit.
-- paq {'ishan9299/nvim-solarized-lua'}                                        -- Solarized with treesitter support

------------------------------- Appearance -------------------------------
paq {'akinsho/nvim-bufferline.lua'}
paq {'hoob3rt/lualine.nvim'}                                                -- Lightweight and simple statusline
paq {'kyazdani42/nvim-web-devicons'}                                        -- Neovim icon font support
paq {'yamatsum/nvim-web-nonicons'}
paq {'lewis6991/gitsigns.nvim'}                                             -- Git add/remove/edit symbols and blame in the virtual text
paq {'norcalli/nvim-colorizer.lua'}                                         -- Highlight the colors in css files
paq {'glepnir/dashboard-nvim'}                                              -- Custom initial page
paq {'karb94/neoscroll.nvim'}                                               -- Improve scrolling

------------------------------- LSP -------------------------------
paq {'nvim-treesitter/nvim-treesitter'}                                     -- Uses the new treesitter alghoritm with lsp support to highlight the syntax
paq {'neovim/nvim-lspconfig'}                                               -- Lsp configuration helpers
paq {'glepnir/lspsaga.nvim'}                                                -- Prettier LSP UI
paq {'folke/lsp-trouble.nvim'}                                              -- List of LSP issues
paq {'jose-elias-alvarez/nvim-lsp-ts-utils'}                                -- Lsp and treesitter utils


--------------------- Automatically install/update/clean Plugins --------------------

install()
update()
clean()

------------------------------- Plugins configuration -------------------------------

g['nnn#layout'] = {
  window = {
    width = 0.9,
    height = 0.6,
    highlight = 'Debug'
  }
}

g['contabs#project#locations'] = {
  { path = '~/projects', depth = 1, git_only = true },
  { path = '~/projects', depth = 2, git_only = true }
}

------------------------------- Lua Plugins configuration ---------------------------

local lualine = require 'lualine'
lualine.setup({
  options = {
    theme = 'tokyonight';
    section_separators = {'', ''},
    component_separators = {'', ''},
    icons_enabled = true,
  },
  sections = {
    lualine_a = { {'mode', upper = true} },
    lualine_b = { {'branch', icon = ''} },
    lualine_c = { {'filename', file_status = true} }
  },
  extensions = { 'fzf' }
})

local compe = require('compe').setup
compe({
  source = {
    path = true;
    buffer = true;
    calc = true;
    nvim_lsp = true;
    nvim_lua = true;
    nvim_treesitter = true;
    spell = true;
    tags = true;
  };
})

local saga = require 'lspsaga'
saga.init_lsp_saga()

local gitsigns = require 'gitsigns'
gitsigns.setup({
  signs = {
    add          = {hl = 'GitSignsAdd'   , text = '+', numhl='GitSignsAddNr'   , linehl='GitSignsAddLn'},
    change       = {hl = 'GitSignsChange', text = '~', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
    delete       = {hl = 'GitSignsDelete', text = '_', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
    topdelete    = {hl = 'GitSignsDelete', text = '‾', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
    changedelete = {hl = 'GitSignsChange', text = '/', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
  },
  current_line_blame = true
})

local bufferline = require 'bufferline'
bufferline.setup {}

local neoscroll = require 'neoscroll'
neoscroll.setup {}

local trouble = require 'trouble'
trouble.setup {}

vim.g.dashboard_default_executive ='fzf'


-- Set git messenger appearence
nvim_exec(
  [[
    hi link gitmessengerHeader Identifier
    hi link gitmessengerHash Comment
    hi link gitmessengerHistory Constant
    hi link gitmessengerPopupNormal CursorLine
    hi gitmessengerEndOfBuffer term=None guifg=None guibg=None ctermfg=None ctermbg=None
  ]],
  false
)

nvim_exec(
  [[
    let $FZF_DEFAULT_COMMAND = 'rg --files --follow'
  ]],
  false
)
