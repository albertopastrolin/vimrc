set encoding=utf8

" Custom commands
source ./viml/custom-commands.vim

" Behaviour modifiers
source ./viml/behaviours.vim

" Key modifiers
source ./viml/keybindings.vim

" Load Plugins
source ./viml/pac.vim

" Appearance
source ./viml/appearence.vim

""" Plugins Settings

" Vim Airline
source ./viml/airline.vim

" Ale
source ./viml/ale-config.vim

" fzf
source ./viml/fzf.vim

" vim-devicons settings
source ./viml/vim-devicons.vim

" Specific settings for vimr
source ./viml/vimr.vim

" Use ripgrep for gitgutter
let g:gitgutter_grep_command = 'rg'
