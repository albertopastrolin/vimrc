let g:lightline = {
      \ 'colorscheme': 'flattened_dark',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             ['gitbranch', 'contab', 'readonly', 'filename', 'modified' ] ],
      \   'right': [ [ 'lineinfo', 'cocstatus', 'currentfunction' ],
      \              [ 'percent' ],
      \              [ 'filetype', 'fileformat' ] ]
      \ },
      \ 'component_function': {
      \   'filetype': 'MyFiletype',
      \   'fileformat': 'MyFileformat',
      \   'gitbranch': 'gitbranch#name',
      \   'contab': 'ContabStatus',
      \   'cocstatus': 'coc#status',
      \   'currentfunction': 'CocCurrentFunction',
      \ },
      \ }

let g:lightline.separator = {
      \   'left': '', 'right': ''
      \}
let g:lightline.subseparator = {
      \   'left': '', 'right': '' 
      \}

let g:lightline.tabline = {
      \   'left': [ ['tabs'] ],
      \   'right': [ ['close'] ]
      \ }

function ContabStatus()
  return contabs#integrations#tabline#raw_label(tabpagenr())
endfunction

function! MyFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
endfunction

function! MyFileformat()
  return winwidth(0) > 70 ? (&fileformat . ' ' . WebDevIconsGetFileFormatSymbol()) : ''
endfunction

function! CocCurrentFunction()
    return get(b:, 'coc_current_function', '')
endfunction

set showtabline=2  " Show tabline
set guioptions-=e  " Don't use GUI tabline
