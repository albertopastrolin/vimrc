let g:ale_linters = {
      \   'javascript': ['eslint'],
      \   'scss': ['stylelint'],
      \   'ruby': ['ruby'],
      \   'css': ['stylelint'],
      \   'erb': ['erb'],
      \   'html': ['htmlhint'],
      \   'vue': ['vls', 'eslint'],
      \}

let g:ale_linter_aliases = {'vue': ['vue', 'javascript']}

" :ALEFix will try and fix your JS code with ESLint.
let g:ale_fixers = {
      \   'javascript': ['eslint'],
      \   'scss': ['stylelint'],
      \   'css': ['stylelint'],
      \}

" ALE Customizzation
let g:ale_sign_column_always = 1
let g:ale_sign_error = "\uF188"
let g:ale_sign_warning = "\uF071"

let g:ale_echo_msg_error_str = "\uF188"
let g:ale_echo_msg_warning_str = "\uF071"
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

" Set the colors of waring/errors
highlight clear ALEErrorSign
highlight clear ALEWarningSign
highlight ALEErrorSign guifg='#cf6a4c'
highlight ALEWarningSign guifg='#d8ad4c'

set synmaxcol=200
