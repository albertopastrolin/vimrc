" Configure the terminal
if &term =~ '256color'
  " disable background color erase
  set t_ut=
endif

" enable 24 bit color support if supported
if (has("termguicolors"))
  set termguicolors
endif

" Set colorscheme
colorscheme flattened_dark
set background=dark

" Force the font to Fira Code + ligatures
if(has("gui_macvim"))
  set guifont=FuraCodeNerdFontComplete-Retina:h14
endif

" Hide the ~ after the EOF
hi NonText guifg=bg

" Git messenger colors
" Header such as 'Commit:', 'Author:'
hi link gitmessengerHeader Identifier

" Commit hash at 'Commit:' header
hi link gitmessengerHash Comment

" History number at 'History:' header
hi link gitmessengerHistory Constant

" Normal color. This color is the most important
hi link gitmessengerPopupNormal CursorLine

" Color of 'end of buffer'. To hide '~' in popup window
hi gitmessengerEndOfBuffer term=None guifg=None guibg=None ctermfg=None ctermbg=None

" Colors for fzf
highlight NormalFloat cterm=NONE ctermfg=14 ctermbg=0 gui=NONE guifg=#93a1a1 guibg=#002931
