set undofile
set undodir=~/.vim/undodir
set clipboard=unnamed
set wildignore+=*/tmp/*,*.so,*.swp,*.zip
set foldmethod=marker
set backspace=indent,eol,start
set ignorecase
set incsearch
set smartcase
set scrolloff=10
set hlsearch!
set wildmode=longest,list,full
set completeopt=longest,menuone

syntax enable
syntax sync minlines=200
autocmd BufEnter * :syntax sync fromstart
set number relativenumber
set wrap
set breakindent
set nolist

set autoindent
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2
set cursorline
set mouse=a

set laststatus=2
set statusline=%F
set wildmenu
set showcmd

set splitbelow
set splitright

set foldmethod=syntax
set nofoldenable

let g:vue_disable_pre_processors=1
filetype plugin indent on
