nnoremap <C-Up> gT
nnoremap <C-Down> gt
nnoremap <C-b> :Buffers<cr>

nnoremap <C-t> :tabnew<CR>
inoremap <C-t> <Esc>:tabnew<CR>i
" This unsets the last search pattern register by hitting return
nnoremap <CR> :noh<CR><CR>

nnoremap <leader>z :SyntaxFix<CR>

" Inserts a placeholder text
nnoremap <leader>l :Loremipsum<CR>

" Nnn binding
nnoremap <leader>f :Np '%:p:h'<CR>

" Used to switch project
nnoremap <silent> <Leader>p :call contabs#project#select()<CR>

" Undotree
nnoremap <silent> <Leader>u :UndotreeToggle<CR>

" Vim Grepper
nnoremap <leader>g :Grepper -tool rg<cr>
