function! PackagerInit() abort
  packadd vim-packager
  " packager is available.
  call packager#init()

  " Additional plugins here.

  " Features
  call packager#add('neoclide/coc.nvim', {'branch': 'release'})                       " Language server support
  call packager#add('w0rp/ale')                                                       " Async linting
  call packager#add('junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' })     " Install fzf for user
  call packager#add('junegunn/fzf.vim')                                               " Fzf vim plugin
  call packager#add('cohama/lexima.vim')                                              " Auto close parentheses
  call packager#add('alvan/vim-closetag', {'for': ['html', 'vue', 'erb']})            " Autclose HTML
  call packager#add('itchyny/vim-cursorword')                                         " Highlight the current word in the buffer
  call packager#add('Shougo/vimproc.vim', {'do' : 'make'})                            " Utility package for unite/fzf
  call packager#add('mcchrish/nnn.vim')                                               " Nnn file manager integration
  call packager#add('junegunn/vim-easy-align')                                        " Alignment shortcuts
  call packager#add('rhysd/git-messenger.vim')                                        " Git commit info directly in vim
  call packager#add('m00qek/nvim-contabs')                                            " Multi project support
  call packager#add('vim-scripts/loremipsum')                                         " Quick lorem ipsum script
  call packager#add('mbbill/undotree')                                                " Undotree
  call packager#add('chaoren/vim-wordmotion')                                         " Better word movements for CamelCase and snake_case
  call packager#add('machakann/vim-sandwich')                                         " Operate on surrounding
  call packager#add('pechorin/any-jump.vim')                                          " Jump to definitions/references

  " Languages
  call packager#add('sheerun/vim-polyglot')                                           " Bundle with lazy loading support of almost every language syntax

  " Appearance
  call packager#add('romainl/flattened')                                              " Solarized, without the bullshit.
  call packager#add('itchyny/lightline.vim')                                          " Lightweight and simple statusline
  call packager#add('ryanoasis/vim-devicons')                                         " Vim icon font support
  call packager#add('mhinz/vim-startify')                                             " Behold the power of moo!
  call packager#add('itchyny/vim-gitbranch')                                          " Git branch name
  call packager#add('airblade/vim-gitgutter')                                         " Git add/remove/edit simbol in the status column
  call packager#add('norcalli/nvim-colorizer.lua')                                    " Highlight the colors in css files

  " Inutilities
  call packager#add('Stoozy/vimcord')

endfunction

" Define user commands for updating/cleaning the plugins.
" Each of them calls PackInit() to load packager and register
" the information of plugins, then performs the task.
command! PackagerInstall call PackagerInit() | call packager#install()
command! -bang PackagerUpdate call PackagerInit() | call packager#update({ 'force_hooks': '<bang>' })
command! PackagerClean call PackagerInit() | call packager#clean()
command! PackagerStatus call PackagerInit() | call packager#status()
