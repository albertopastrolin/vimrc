let g:webdevicons_enable = 1
let g:webdevicons_enable_vimfiler = 1
let g:webdevicons_enable_airline_tabline = 1
let g:webdevicons_enable_airline_statusline = 1
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols = {
      \ 'vue': "\ue618",
      \ 'woff': "\uf031",
      \ 'woff2': "\uf031",
      \ 'ttf': "\uf031",
      \ 'eot': "\uf031",
      \ 'svg': "\ue75e",
\ }
let g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols = {
      \ '.*html.*\.*$': "\ue736",
\ }
